/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.stream.app;

import com.sun.jersey.api.core.PackagesResourceConfig;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author ravage
 */
@ApplicationPath("/api")
public class App extends PackagesResourceConfig {

    public App() {
        super("me.stream.resources");
    }

    @Override
    public Map<String, MediaType> getMediaTypeMappings() {
        Map<String, MediaType> mediaTypes = new HashMap<String, MediaType>();
        mediaTypes.put("xml", MediaType.APPLICATION_XML_TYPE);
        mediaTypes.put("json", MediaType.APPLICATION_JSON_TYPE);
        return mediaTypes;
    }
}