/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.stream.entities;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author ravage
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCollection", propOrder = {
    "users"
})

@XmlRootElement(name = "UserCollection")
public class UserCollection {
    @XmlElementWrapper
    private List<User> users;
    
    public UserCollection() {
        users = new ArrayList<User>();
    }

    /**
     * @return the streams
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * @param streams the streams to set
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }
    
    public void add(User user) {
        users.add(user);
    }
    
}
