/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.stream.models;

import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.SearchResultEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.stream.entities.MediaStream;
import me.stream.entities.MediaStreamCollection;
import me.stream.entities.User;
import me.stream.ldap.LDAP;

/**
 *
 * @author ravage
 */
public class UserModel {

    protected static final String host = "192.168.100.2";
    protected static final int port = 389;
    protected static final String baseDn = "dc=stream,dc=me";
    protected static final String password = "mocoto";
    protected static final String loginAdmin = "admin";
    protected LDAP ldap;

    public UserModel() {
        ldap = new LDAP(host, port);
    }

    public User getUser(String cn) {
        bindAdmin(loginAdmin, password);
        List<SearchResultEntry> entries = ldap.search(String.format("ou=Users,%s", baseDn), String.format("cn=%s", cn));
        ldap.close();

        if (entries.isEmpty()) {
            return null;
        }

        return buildUser(entries.get(0));
    }

    public User getUserByToken(String token) {
        bindAdmin(loginAdmin, password);
        List<SearchResultEntry> entries = ldap.search(String.format("ou=Users,%s", baseDn), String.format("description=%s", token));
        ldap.close();

        if (entries.isEmpty() || entries.size() > 1) {
            return null;
        }

        return buildUser(entries.get(0));
    }

    public List<String> getMemberships() {
        return new ArrayList<String>();
    }

    public User login(String login, String password, String host) {
        if (!bindUser(login, password)) {
            return null;
        }

        List<SearchResultEntry> entries = ldap.search(String.format("ou=Users,%s", baseDn), String.format("&(cn=%s)(shadowInactive=0)", login));

        if (entries == null || entries.isEmpty()) {
            return null;
        }

        String token = UUID.randomUUID().toString();

        bindAdmin(loginAdmin, UserModel.password);
        
        ldap.modify(String.format("cn=%s,ou=Users,%s", login, baseDn), "shadowLastChange", String.valueOf(System.currentTimeMillis() / 1000));
        ldap.modify(String.format("cn=%s,ou=Users,%s", login, baseDn), "description", token);
        ldap.modify(String.format("cn=%s,ou=Devices,%s", login, baseDn), "ipHostNumber", host);

        ldap.close();

        return buildUser(entries.get(0), token);
    }

    public User logout(String token) {
        User user = getUserByToken(token);

        if (user == null || "-".equals(user.getToken())) {
            return null;
        }

        bindAdmin(loginAdmin, password);
        ldap.modify(String.format("cn=%s,ou=Users,%s", user.getUsername(), baseDn), "description", "-");
        ldap.close();
        return user;
    }

    public MediaStreamCollection getStreams(String token) {
        User user = getUserByToken(token);

        if (user == null) {
            return null;
        }

        bindAdmin(loginAdmin, password);
        List<SearchResultEntry> entries = ldap.search(String.format("ou=Profiles,%s", baseDn), String.format("memberUid=%s", user.getUsername()));
        ldap.close();

        MediaStreamCollection streams = buildStreams(entries);

        return streams;
    }
    
    public MediaStream getStream(String token, int id) {
        User user = getUserByToken(token);

        if (user == null) {
            return null;
        }

        bindAdmin(loginAdmin, password);
        List<SearchResultEntry> entries = ldap.search(String.format("ou=Profiles,%s", baseDn), 
                String.format("&(memberUid=%s)(gidNumber=%d)", user.getUsername(), id));
        ldap.close();

        if (entries == null || entries.isEmpty())
            return null;
            
        
        return buildStream(entries.get(0));
    }

    protected MediaStreamCollection buildStreams(List<SearchResultEntry> entries) {
        MediaStreamCollection streams = new MediaStreamCollection();

        for (SearchResultEntry entry : entries) {
            streams.add(buildStream(entry));
        }

        return streams;

    }

    protected MediaStream buildStream(SearchResultEntry entry) {
        MediaStream stream = new MediaStream();

        stream.setKey(entry.getAttributeValue("description"));
        stream.setName(entry.getAttributeValue("cn"));
        stream.setUrl(entry.getAttributeValue("ipHostNumber"));
        stream.setId(entry.getAttributeValue("gidNumber"));

        return stream;
    }

    protected boolean bindAdmin(String login, String password) {
        try {
            ldap.connect();
            return ldap.bind(String.format("cn=%s,%s", login, baseDn), password);
        } catch (LDAPException ex) {
            Logger.getLogger(UserModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    protected boolean bindUser(String login, String password) {
        try {
            ldap.connect();
            return ldap.bind(String.format("cn=%s,ou=Users,%s", login, baseDn), password);
        } catch (LDAPException ex) {
            Logger.getLogger(UserModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    protected User buildUser(SearchResultEntry entry) {
        User user = new User();
        user.setEmail(entry.getAttributeValue("mail"));
        user.setFirstname(entry.getAttributeValue("givenName"));
        user.setLastname(entry.getAttributeValue("sn"));
        user.setUsername(entry.getAttributeValue("cn"));
        user.setToken(entry.getAttributeValue("token"));
        user.setStatus(true);
        
        bindAdmin(loginAdmin, password);
        List<SearchResultEntry> entries = ldap.search(String.format("ou=Profiles,%s", baseDn), String.format("memberUid=%s", user.getUsername()));
        ldap.close();
        user.setMemberships(buildMemberships(entries));
        return user;
    }
    
    protected List<String> buildMemberships(List<SearchResultEntry> entries) {
        List<String> memberships = new ArrayList<String>();
        
        for (SearchResultEntry entry : entries){
            memberships.add(entry.getAttributeValue("cn"));
        }
        
        return memberships;
    }

    protected User buildUser(SearchResultEntry entry, String token) {
        User user = buildUser(entry);
        user.setToken(token);
        return user;
    }
}
