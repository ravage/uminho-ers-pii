/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.stream.models;

import com.unboundid.ldap.sdk.SearchResultEntry;
import java.util.List;
import java.util.UUID;
import me.stream.entities.MediaStream;
import me.stream.entities.MediaStreamCollection;
import me.stream.entities.User;
import me.stream.entities.UserCollection;

/**
 *
 * @author ravage
 */
public class AdminModel extends UserModel {

    public AdminModel() {
        super();
    }
    
    @Override
    public User login(String login, String password, String host) {
        if (!bindUser(login, password)) {
            return null;
        }

        List<SearchResultEntry> entries = ldap.search(String.format("ou=Groups,%s", baseDn), String.format("(&(cn=Admin)(memberUid=%s))", login));
         
        if (entries == null || entries.isEmpty()) {
            return null;
        }
        
        entries = ldap.search(String.format("ou=Users,%s", baseDn), String.format("(cn=%s)", login));

        String token = UUID.randomUUID().toString();

        ldap.close();

        return buildUser(entries.get(0), token);
    }
    
    public UserCollection getUsers() {
        bindAdmin(loginAdmin, password);
        List<SearchResultEntry> entries = ldap.search(String.format("ou=Users,%s", baseDn), "objectClass=inetOrgPerson");
        ldap.close();

        UserCollection users = new UserCollection();
        if (entries.isEmpty()) {
            return users;
        }

        for (SearchResultEntry entry : entries) {
            users.add(buildUser(entry));
        }

        return users;
    }
    
    
    public MediaStreamCollection getStreams() {
        bindAdmin(loginAdmin, password);
        List<SearchResultEntry> entries = ldap.search(String.format("ou=Profiles,%s", baseDn), "(objectClass=*)");
        ldap.close();

        MediaStreamCollection streams = buildStreams(entries);

        return streams;
    }
    
    public MediaStreamCollection getSubscribedStreams(String user) {
        bindAdmin(loginAdmin, password);
        List<SearchResultEntry> entries = ldap.search(String.format("ou=Profiles,%s", baseDn), String.format("(memberUid=%s)", user));
        ldap.close();

        MediaStreamCollection streams = buildStreams(entries);

        return streams;
    }
    
    public MediaStream getStream(int id) {
        bindAdmin(loginAdmin, password);
        List<SearchResultEntry> entries = ldap.search(String.format("ou=Profiles,%s", baseDn), 
                String.format("(gidNumber=%d)", id));
        ldap.close();

        if (entries == null || entries.isEmpty())
            return null;
            
        
        return buildStream(entries.get(0));
    }

    public void removeStreamForUser(String username, int streamId) {
        MediaStream stream = getStream(streamId);
        bindAdmin(loginAdmin, password);    
        ldap.delete(String.format("cn=%s,ou=Profiles,%s", stream.getName(), baseDn), "memberUid", username);
        ldap.close();
    }
    
    public void addStreamForUser(String username, int streamId) {
        MediaStream stream = getStream(streamId);
        bindAdmin(loginAdmin, password); 
        ldap.add(String.format("cn=%s,ou=Profiles,%s", stream.getName(), baseDn), "memberUid", username);
        ldap.close();
    }
    
    public void activate(String user) {
        bindAdmin(loginAdmin, password); 
        ldap.modify(String.format("cn=%s,ou=Users,%s", user, baseDn), "shadowInactive", "0");
        ldap.close();
    }
    
    public void deactivate(String user) {
        bindAdmin(loginAdmin, password); 
        ldap.modify(String.format("cn=%s,ou=Users,%s", user, baseDn), "shadowInactive", "1");
        ldap.close();
    }
    
    public void delete(String user) {
        MediaStreamCollection streams = getSubscribedStreams(user);
        bindAdmin(loginAdmin, password); 
        ldap.delete(String.format("cn=%s,ou=Users,%s", user, baseDn));
        ldap.delete(String.format("cn=%s,ou=Devices,%s", user, baseDn));
        for (MediaStream stream : streams.getStreams())
            ldap.delete(String.format("cn=%s,ou=Profiles,%s", stream.getName(), baseDn), "memberUid", user);
        ldap.close();
    }
    
}