/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.stream.resources;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import me.stream.entities.MediaStream;
import me.stream.entities.MediaStreamCollection;
import me.stream.entities.User;
import me.stream.entities.UserCollection;
import me.stream.models.AdminModel;
import me.stream.models.UserModel;

/**
 *
 * @author ravage
 */
@Path("/admin")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class AdminResource {
    private final AdminModel model;
    @Context
    private HttpServletRequest request;
    
    public AdminResource() {
        model = new AdminModel();
    }
    
    @POST @Path("/login")
    public User login(@FormParam("login") String login, @FormParam("password") String password) {
        User user = model.login(login, password, request.getRemoteAddr());
        return (user == null) ? new User() : user;
    }
    
    @GET @Path("/{token}/logout")
    public User logout(@PathParam("token") String token) {
        User user = model.logout(token);
        return (user == null) ? new User() : user;
    }
    
    @GET @Path("/streams")
    public MediaStreamCollection streams() {
        return model.getStreams();
    }
    
    @GET @Path("/streams/{user}")
    public MediaStreamCollection streams(@PathParam("user") String user) {
        return model.getSubscribedStreams(user);
    }
    
    /*@GET @Path("/stream/{id}")
    public MediaStream getStream(@PathParam("id") int id) {
        MediaStream result = model.getStream(id);
        return (result == null) ? new MediaStream() : result;
    }*/
    
    @GET @Path("/users")
    public UserCollection getUsers() {
        UserCollection result = model.getUsers();
        return result;
    }
    
    @DELETE @Path("/users/{username}/streams/{stream}")
    public void removeStreamForUser(@PathParam("username") String username, @PathParam("stream") int stream) {
        model.removeStreamForUser(username, stream);
    }
    
    @POST @Path("/users/{username}/streams/{stream}")
    public void addStreamForUser(@PathParam("username") String username, @PathParam("stream") int stream) {
        model.addStreamForUser(username, stream);
    }
    
    @PUT @Path("/users/{username}/activate")
    public void activate(@PathParam("username") String username) {
        model.activate(username);
    }
    
    @PUT @Path("/users/{username}/deactivate")
    public void deactivate(@PathParam("username") String username) {
        model.deactivate(username);
    }
    
    @DELETE @Path("/users/{username}")
    public void deleteAccount(@PathParam("username") String username) {
        model.delete(username);
    }
}
