/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.stream.resources;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import me.stream.entities.MediaStream;
import me.stream.entities.MediaStreamCollection;
import me.stream.entities.User;
import me.stream.models.UserModel;

/**
 *
 * @author ravage
 */
@Path("/users")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class UserResource {
    private final UserModel model;
    @Context
    private HttpServletRequest request;
    
    public UserResource() {
        model = new UserModel();
    }
    
    @POST @Path("/login")
    public User login(@FormParam("login") String login, @FormParam("password") String password) {
        User user = model.login(login, password, request.getRemoteAddr());
        return (user == null) ? new User() : user;
    }
    
    @GET @Path("/{token}/logout")
    public User logout(@PathParam("token") String token) {
        User user = model.logout(token);
        return (user == null) ? new User() : user;
    }
    
    @GET @Path("/{token}/streams")
    public MediaStreamCollection streams(@PathParam("token") String token) {
        return model.getStreams(token);
    }
    
    @GET @Path("/{token}/stream/{id}")
    public MediaStream getStream(@PathParam("token") String token, @PathParam("id") int id) {
        MediaStream result = model.getStream(token, id);
        return (result == null) ? new MediaStream() : result;
    }
}
