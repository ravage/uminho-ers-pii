import me.stream.player.PlayerPanel;

import javax.swing.SwingUtilities;

/**
 * Created by IntelliJ IDEA.
 * me.stream.entities.User: ravage
 * Date: 01/06/2011
 * Time: 17:44
 * To change this template use File | Settings | File Templates.
 */
public class App {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new PlayerPanel());
    }
}