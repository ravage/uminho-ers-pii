package me.stream.player;

import me.stream.entities.MediaStream;
import me.stream.entities.MediaStreamCollection;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.*;
import java.util.List;


import static javax.swing.ListSelectionModel.SINGLE_SELECTION;

/**
 * Created by IntelliJ IDEA.
 * me.stream.entities.User: ravage
 * Date: 01/06/2011
 * Time: 17:30
 * To change this template use File | Settings | File Templates.
 */
public class PlayerPanel implements Runnable {
    private JPanel panel;
    private JTable tblStreams;
    private JButton btnLogin;
    private JButton btnLogout;
    private JTextField txtVLC;
    private JButton btnRefresh;
    private JButton btnVLC;
    private final DefaultTableModel modelStreams;

    private final ServiceClient client;

    public PlayerPanel() {
        client = new ServiceClient();
        modelStreams = new DisableEditModel();
    }

    public void run() {
        setBorder(tblStreams);
        tblStreams.setModel(modelStreams);
        tblStreams.setSelectionMode(SINGLE_SELECTION);
        tblStreams.setAutoCreateColumnsFromModel(true);

        modelStreams.addColumn("Group");
        modelStreams.addColumn("Stream");
        tblStreams.addMouseListener(new TableStreamsController());

        //bind events
        btnLogin.addActionListener(new LoginController());
        btnLogout.addActionListener(new LogoutController());
        btnRefresh.addActionListener(new GetStreamController());
        btnVLC.addActionListener(new VLCPathController());

        btnRefresh.setEnabled(false);
        btnLogout.setEnabled(false);

        File file = new File("cfg.ini");
        if (file.exists()) {
            try {
                FileInputStream stream = new FileInputStream(file);
                DataInputStream ds = new DataInputStream(stream);
                BufferedReader br = new BufferedReader(new InputStreamReader(ds));
                txtVLC.setText(br.readLine());

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        JFrame frame = new JFrame();
        frame.add(panel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(600, 400);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);


    }

    private void setBorder(JComponent component) {
        component.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(3, 3, 3, 3)));
    }

    private void refreshStreams(MediaStreamCollection streams) {
        modelStreams.setRowCount(0);
        for(MediaStream stream : streams.getStreams()) {
            modelStreams.addRow(new Object[] { stream, stream.getUrl() });
        }
    }

   private void launchStream(MediaStream stream) {
       if (stream.getId().equals("-1"))
           return;

       if (!client.isAuthorized(stream)) {
           refreshStreams(client.getStreams());
           return;
       }

       List<String> args = new ArrayList<String>();

       if(System.getProperty("os.name").toLowerCase().contains("mac")) {
            args.add(String.format("%s/Contents/MacOS/VLC", txtVLC.getText()));
       }
       else {
            args.add(txtVLC.getText());
       }

       args.add(stream.getUrl());
       args.add((String.format("--ts-csa-ck=%s", stream.getKey())));

        ProcessBuilder vlc = new ProcessBuilder(args);
       try {
           vlc.start();
       } catch (IOException e) {
           e.printStackTrace();
       }
   }

    private class DisableEditModel extends DefaultTableModel {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    private class LoginController implements ActionListener, ILoginListener {
        public void actionPerformed(ActionEvent e) {
            LoginForm loginForm = new LoginForm(this);
            loginForm = new LoginForm(this);
            loginForm.pack();
            loginForm.setLocationRelativeTo(panel);
            loginForm.setVisible(true);
        }

        public void okPressed(String login, char[] password) {
            if (client.login(login, new String(password))) {
                refreshStreams(client.getStreams());

                btnRefresh.setEnabled(true);
                btnLogout.setEnabled(true);
                btnLogin.setEnabled(false);
            }
            for (int i = 0; i < password.length; i++)
                password[i] = 0;
        }
    }

    private class LogoutController implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            client.logout();
            btnRefresh.setEnabled(false);
            btnLogout.setEnabled(false);
            btnLogin.setEnabled(true);
            modelStreams.setRowCount(0);
        }
    }

    private class GetStreamController implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            refreshStreams(client.getStreams());
        }
    }

    private class VLCPathController implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            JFileChooser dialog = new JFileChooser();
            int result = dialog.showOpenDialog(panel.getParent());

            if (result != JFileChooser.APPROVE_OPTION)
                return;

            File file = dialog.getSelectedFile();
            txtVLC.setText(file.getAbsolutePath());
            try {
                FileWriter writer = new FileWriter("cfg.ini");
                writer.write(file.getAbsolutePath());
                writer.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private class TableStreamsController implements MouseListener {
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() != 2)
                return;

            JTable table = (JTable)e.getSource();
            MediaStream stream = (MediaStream)table.getValueAt(table.getSelectedRow(), 0);
            launchStream(stream);
        }

        public void mousePressed(MouseEvent e) {}

        public void mouseReleased(MouseEvent e) {}

        public void mouseEntered(MouseEvent e) {}

        public void mouseExited(MouseEvent e) {}
    }


}
