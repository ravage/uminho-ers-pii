package me.stream.player;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;
import com.sun.jersey.client.apache.ApacheHttpClient;
import me.stream.entities.MediaStream;
import me.stream.entities.MediaStreamCollection;
import me.stream.entities.User;

import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 02/06/2011
 * Time: 20:50
 * To change this template use File | Settings | File Templates.
 */
public class ServiceClient {
    private final static String BASE_URL = "http://api.stream.me/StreamMeAPI/api";
    private WebResource userResource;
    private User user;

    public ServiceClient() {
        Client client = ApacheHttpClient.create();
        userResource = client.resource(BASE_URL + "/users");
    }

    public boolean login(String login, String password) {
        MultivaluedMap<String, String> params = new Form();
        params.add("login", login);
        params.add("password", password);
        user = userResource.path("login").post(User.class, params);

        return user.getStatus();
    }

    public boolean logout() {
        user = userResource.path(user.getToken()).path("logout").get(User.class);
        return user.getStatus();
    }

    public MediaStreamCollection getStreams() {
        return userResource.path(user.getToken()).path("streams").get(MediaStreamCollection.class);
    }

    public boolean isAuthorized(MediaStream stream) {
        MediaStream result = userResource.path(user.getToken()).path("stream").path(stream.getId()).get(MediaStream.class);
        return result.getName().equals(stream.getName());
    }
}
