package me.stream.admin;

import me.stream.entities.MediaStream;
import me.stream.entities.MediaStreamCollection;
import me.stream.entities.User;
import me.stream.entities.UserCollection;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import static javax.swing.ListSelectionModel.SINGLE_SELECTION;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 03/06/2011
 * Time: 02:16
 * To change this template use File | Settings | File Templates.
 */
public class StreamMeForm implements Runnable {
    private JTabbedPane tabbedPane1;
    private JTable tblUsers;
    private JButton btnAddGroup;
    private JButton btnRemoveGroup;
    private JButton btnRemoveAccount;
    private JPanel panel;
    private JButton btnLogout;
    private JButton btnActivateAccount;
    private JButton btnDeactivateAccount;
    private JButton btnRefresh;
    private JButton btnLogin;
    private JTable tblStreamsSubscribed;
    private JTable tblStreamsAvailable;

    private final ServiceClient client;
    private final DefaultTableModel modelStreamsAvailable;
    private final DefaultTableModel modelStreamsSubscribed;
    private final DefaultTableModel modelUsers;

    public StreamMeForm() {
        client = new ServiceClient();
        modelStreamsAvailable = new DisableEditModel();
        modelUsers  = new DisableEditModel();
        modelStreamsSubscribed = new DisableEditModel();
    }

    private class DisableEditModel extends DefaultTableModel {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public void run() {
        setBorder(tblStreamsAvailable);
        setBorder(tblUsers);
        setBorder(tblStreamsSubscribed);

        tblStreamsAvailable.setModel(modelStreamsAvailable);
        tblStreamsAvailable.setSelectionMode(SINGLE_SELECTION);
        tblStreamsAvailable.setAutoCreateColumnsFromModel(true);
        tblStreamsAvailable.setName("tblStreamsAvailable");

        tblStreamsSubscribed.setModel(modelStreamsSubscribed);
        tblStreamsSubscribed.setSelectionMode(SINGLE_SELECTION);
        tblStreamsSubscribed.setAutoCreateColumnsFromModel(true);
        tblStreamsSubscribed.setName("tblStreamsSubscribed");

        tblUsers.setModel(modelUsers);
        tblUsers.setSelectionMode(SINGLE_SELECTION);
        tblUsers.setAutoCreateColumnsFromModel(true);
        tblUsers.setName("tblUsers");

        modelStreamsAvailable.addColumn("Availabe");
        modelStreamsSubscribed.addColumn("Subscribed");
        modelUsers.addColumn("Users");

        // bindings
        btnLogin.addActionListener(new LoginController());
        btnRemoveGroup.addActionListener(new RemoveGroupController());
        btnAddGroup.addActionListener(new AddGroupController());
        btnActivateAccount.addActionListener(new ActivateAccountController());
        btnDeactivateAccount.addActionListener(new DeactivateAccountController());
        btnRemoveAccount.addActionListener(new RemoveAccountController());
        btnRefresh.addActionListener(new RefreshController());
        btnLogout.addActionListener(new LogoutController());

        tblUsers.addMouseListener(new TableStreamsController());

        btnRefresh.setEnabled(false);
        btnRemoveGroup.setEnabled(false);
        btnAddGroup.setEnabled(false);
        btnActivateAccount.setEnabled(false);
        btnDeactivateAccount.setEnabled(false);
        btnRemoveAccount.setEnabled(false);
        btnLogout.setEnabled(false);

        JFrame frame = new JFrame();
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 520);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);
    }

    private void refreshStreams(MediaStreamCollection streams, DefaultTableModel model) {
        model.setRowCount(0);
        for(MediaStream stream : streams.getStreams()) {
            model.addRow(new Object[] { stream });
        }
    }

    private void refreshUsers(UserCollection users) {
        modelUsers.setRowCount(0);
        for(User user : users.getUsers()) {
            modelUsers.addRow(new Object[] { user });
        }
    }



    private void setBorder(JComponent component) {
        component.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                BorderFactory.createEmptyBorder(3, 3, 3, 3)));
    }



    private class AddGroupController implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            int rowAvailable = tblStreamsAvailable.getSelectedRow();
            int rowUser = tblUsers.getSelectedRow();

            if (rowAvailable == -1 || rowUser == -1)
                return;

            MediaStream stream = (MediaStream)tblStreamsAvailable.getValueAt(rowAvailable, 0);
            User user = (User)tblUsers.getValueAt(rowUser, 0);
            client.addStreamForUser(user.getUsername(), stream.getId());
            refreshStreams(client.getSubscribedStreams(user.getUsername()), modelStreamsSubscribed);
        }
    }

    private class RemoveGroupController implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            int rowSubscribed = tblStreamsSubscribed.getSelectedRow();
            int rowUser = tblUsers.getSelectedRow();

            if (rowSubscribed == -1 || rowUser == -1)
                return;

            MediaStream stream = (MediaStream)tblStreamsSubscribed.getValueAt(rowSubscribed, 0);
            User user = (User)tblUsers.getValueAt(rowUser, 0);
            client.removeStreamForUser(user.getUsername(), stream.getId());
            refreshStreams(client.getSubscribedStreams(user.getUsername()), modelStreamsSubscribed);
        }
    }

    private class RemoveAccountController implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            int rowUser = tblUsers.getSelectedRow();

            if (rowUser == -1)
                return;

            User user = (User)tblUsers.getValueAt(rowUser, 0);
            client.delete(user.getUsername());
            modelStreamsSubscribed.setNumRows(0);
            modelUsers.removeRow(rowUser);
        }
    }

    private class ActivateAccountController implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            int rowUser = tblUsers.getSelectedRow();

            if (rowUser == -1)
                return;

            User user = (User)tblUsers.getValueAt(rowUser, 0);
            client.activate(user.getUsername());
        }
    }

    private class DeactivateAccountController implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            int rowUser = tblUsers.getSelectedRow();

            if (rowUser == -1)
                return;

            User user = (User)tblUsers.getValueAt(rowUser, 0);
            client.deactivate(user.getUsername());
        }
    }

    private class LoginController implements ActionListener, ILoginListener {

        public void actionPerformed(ActionEvent e) {
            LoginForm loginForm = new LoginForm(this);
            loginForm = new LoginForm(this);
            loginForm.pack();
            loginForm.setLocationRelativeTo(panel);
            loginForm.setVisible(true);
        }

        public void okPressed(String login, char[] password) {
            if (client.login(login, new String(password))) {
                refreshStreams(client.getAvailableStreams(), modelStreamsAvailable);
                refreshUsers(client.getUsers());
                btnRefresh.setEnabled(true);
                btnRemoveGroup.setEnabled(true);
                btnAddGroup.setEnabled(true);
                btnActivateAccount.setEnabled(true);
                btnDeactivateAccount.setEnabled(true);
                btnRemoveAccount.setEnabled(true);
                btnLogout.setEnabled(true);
                btnLogin.setEnabled(false);
            }
            for (int i = 0; i < password.length; i++)
                password[i] = 0;

        }
    }

    private class LogoutController implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            btnRefresh.setEnabled(false);
            btnRemoveGroup.setEnabled(false);
            btnAddGroup.setEnabled(false);
            btnActivateAccount.setEnabled(false);
            btnDeactivateAccount.setEnabled(false);
            btnRemoveAccount.setEnabled(false);
            btnLogout.setEnabled(false);
            btnLogin.setEnabled(true);

            modelStreamsAvailable.setRowCount(0);
            modelStreamsSubscribed.setRowCount(0);
            modelUsers.setRowCount(0);
        }
    }

    private class RefreshController implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            modelStreamsSubscribed.setRowCount(0);
            refreshStreams(client.getAvailableStreams(), modelStreamsAvailable);
            refreshUsers(client.getUsers());
        }
    }

    private class TableStreamsController implements MouseListener {
         public void mouseClicked(MouseEvent e) {
             if (e.getClickCount() != 2)
                 return;

             JTable table = (JTable)e.getSource();

             if (table.getName().equals("tblUsers")) {
                 User user = (User)table.getValueAt(table.getSelectedRow(), 0);
                 refreshStreams(client.getSubscribedStreams(user.getUsername()), modelStreamsSubscribed);
             }
         }

         public void mousePressed(MouseEvent e) {}

         public void mouseReleased(MouseEvent e) {}

         public void mouseEntered(MouseEvent e) {}

         public void mouseExited(MouseEvent e) {}
     }

}
