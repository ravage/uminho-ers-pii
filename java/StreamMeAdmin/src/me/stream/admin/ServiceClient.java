package me.stream.admin;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.api.representation.Form;
import com.sun.jersey.client.apache.ApacheHttpClient;
import me.stream.entities.MediaStream;
import me.stream.entities.MediaStreamCollection;
import me.stream.entities.User;
import me.stream.entities.UserCollection;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;

import static com.sun.jersey.api.json.JSONConfiguration.FEATURE_POJO_MAPPING;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 02/06/2011
 * Time: 20:50
 * To change this template use File | Settings | File Templates.
 */
public class ServiceClient {
    private final static String BASE_URL = "http://192.168.100.3:8080/StreamMeAPI/api";
    private WebResource userResource;
    private User user;

    public ServiceClient() {
        ClientConfig clientConfig = new DefaultClientConfig();
        Client client = ApacheHttpClient.create(clientConfig);
        userResource = client.resource(BASE_URL + "/admin");
    }

    public boolean login(String login, String password) {
        MultivaluedMap<String, String> params = new Form();
        params.add("login", login);
        params.add("password", password);
        user = userResource.path("login").post(User.class, params);

        return user.getStatus();
    }

    public boolean logout() {
        user = userResource.path(user.getToken()).path("logout").get(User.class);
        return user.getStatus();
    }

    public MediaStreamCollection getAvailableStreams() {
        return userResource.path("streams").get(MediaStreamCollection.class);
    }

    public MediaStreamCollection getSubscribedStreams(String user) {
        return userResource.path("streams").path(user).get(MediaStreamCollection.class);
    }

    public UserCollection getUsers() {
        return userResource.path("users").get(UserCollection.class);
    }

    public void removeStreamForUser(String username, String streamId) {
        userResource.path("users").path(username).path("streams").path(streamId).delete();
    }

    public void addStreamForUser(String username, String streamId) {
        userResource.path("users").path(username).path("streams").path(streamId).post();
    }

    public void activate(String username) {
        userResource.path("users").path(username).path("activate").put();
    }

    public void deactivate(String username) {
        userResource.path("users").path(username).path("deactivate").put();
    }

    public boolean isAuthorized(MediaStream stream) {
        MediaStream result = userResource.path(user.getUsername()).path("streams").path(stream.getId()).get(MediaStream.class);
        return result.getName().equals(stream.getName());
    }

    public void delete(String username) {
        userResource.path("users").path(username).delete();
    }
}
