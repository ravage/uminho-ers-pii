package me.stream.app;

import me.stream.admin.StreamMeForm;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 03/06/2011
 * Time: 02:28
 * To change this template use File | Settings | File Templates.
 */
public class App {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new StreamMeForm());
    }
}
