package me.stream.entities;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author ravage
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "me.stream.entities.MediaStreamCollection", propOrder = {
    "streams"
})

@XmlRootElement(name = "me.stream.entities.MediaStreamCollection")
public class MediaStreamCollection {
    @XmlElementWrapper
    private List<MediaStream> streams;
    
    public MediaStreamCollection() {
        streams = new ArrayList<MediaStream>();
    }

    /**
     * @return the streams
     */
    public List<MediaStream> getStreams() {
        return streams;
    }

    /**
     * @param streams the streams to set
     */
    public void setStreams(List<MediaStream> streams) {
        this.streams = streams;
    }
    
    public void add(MediaStream stream) {
        streams.add(stream);
    }
}
