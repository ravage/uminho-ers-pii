/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.stream.ldap;

import com.unboundid.ldap.sdk.AddRequest;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.BindResult;
import com.unboundid.ldap.sdk.Control;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.DeleteRequest;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.LDAPResult;
import com.unboundid.ldap.sdk.Modification;
import com.unboundid.ldap.sdk.ModifyRequest;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
import com.unboundid.ldif.LDIFException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ravage
 */
public class LDAP {

    private final LDAPConnection connection;
    private final String host;
    private final int port;
    private LDAPResult bindResult;

    public LDAP(String host, int port) {
        connection = new LDAPConnection();
        this.host = host;
        this.port = port;
    }

    public void connect() throws LDAPException {
        connection.connect(host, port);
    }

    public boolean bind(String dn, String password) {
        try {
            bindResult = connection.bind(dn, password);
            return bindResult.getResultCode() == ResultCode.SUCCESS;
        } catch (LDAPException ex) {
            Logger.getLogger(LDAP.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public LDAPResult getBindResult() {
        return bindResult;
    }

    public List<SearchResultEntry> search(String baseDn, String search) {
        try {
            Filter filter = Filter.create(search);
            SearchRequest request = new SearchRequest(baseDn, SearchScope.SUB, filter, null);
            SearchResult result = connection.search(request);
            List<SearchResultEntry> entries = result.getSearchEntries();

            return entries;
            //int count = result.getEntryCount();
        } catch (LDAPException ex) {
            Logger.getLogger(LDAP.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean modify(String dn, String attribute, String value) {
        try {
            ModifyRequest modifyRequest = new ModifyRequest(
                    String.format("dn: %s", dn),
                    "changetype: modify",
                    String.format("replace: %s", attribute),
                    String.format("%s: %s", attribute, value));

            bindResult = connection.modify(modifyRequest);
            return bindResult.getResultCode() == ResultCode.SUCCESS;
        } catch (LDAPException ex) {
            Logger.getLogger(LDAP.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (LDIFException ex) {
            Logger.getLogger(LDAP.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean delete(String dn, String attribute, String value) {
        try {
            ModifyRequest modifyRequest = new ModifyRequest(
                    String.format("dn: %s", dn),
                    "changetype: modify",
                    String.format("delete: %s", attribute),
                    String.format("%s: %s", attribute, value));

            bindResult = connection.modify(modifyRequest);
            return bindResult.getResultCode() == ResultCode.SUCCESS;
        } catch (LDAPException ex) {
            Logger.getLogger(LDAP.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (LDIFException ex) {
            Logger.getLogger(LDAP.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean delete(String dn) {
        try {
            DeleteRequest deleteRequest = new DeleteRequest(dn);
            bindResult = connection.delete(deleteRequest);
            return bindResult.getResultCode() == ResultCode.SUCCESS;
        } catch (LDAPException ex) {
            Logger.getLogger(LDAP.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean add(String dn, String attribute, String value) {
        try {
            ModifyRequest modifyRequest = new ModifyRequest(
                    String.format("dn: %s", dn),
                    "changetype: modify",
                    String.format("add: %s", attribute),
                    String.format("%s: %s", attribute, value));

            bindResult = connection.modify(modifyRequest);
            return bindResult.getResultCode() == ResultCode.SUCCESS;
        } catch (LDAPException ex) {
            Logger.getLogger(LDAP.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (LDIFException ex) {
            Logger.getLogger(LDAP.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean isConnected() {
        return connection.isConnected();
    }

    public void close() {
        connection.close();
    }
}