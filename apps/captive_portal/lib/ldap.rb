require 'net/ldap'

module PI
  class LDAP

    attr_accessor :host, :user, :password

    def initialize(host, user = '', password = '')
      @host = host
      @user = user
      @password = password

      @ldap = Net::LDAP.new
      @ldap.host = @host
    end

    def bind()
      @ldap.authenticate(@user, @password)
      @ldap.bind
    end

    def add(dn, attrs)
      @ldap.add(:dn => dn, :attributes => attrs)
      @ldap.get_operation_result
    end

    def delete(dn)
      @ldap.delete(:dn => dn)
    end

    def connection
      @ldap
    end
  end
end
