class User
  @@attributes = [:password, :username, :firstname, :lastname, :email]

  attr_accessor *@@attributes

  def initialize(params = {})
    @host = '192.168.100.2'
    @base_dn = 'ou=Users,dc=stream,dc=me'

    @username = params['login'] 
    @password = params['password'] if params.has_key?('password')
    self.firstname = params['name'] if params.has_key?('name')
    self.lastname = params['name'] if params.has_key?('name')
    @email = params['email'] if params.has_key?('email')
    @ip = params['ip'] if params.has_key?('ip')
    @ldap = PI::LDAP.new(@host)
  end

  def authenticate()
    @ldap.user = self.username
    @ldap.password = @password
    @ldap.bind()
  end

  def authenticate_admin
    @ldap.user = 'cn=admin,dc=stream,dc=me'
    @ldap.password = 'mocoto'
    @ldap.bind()
  end

  def username
    "cn=#{@username},#{@base_dn}"
  end

  def uid
   @username 
  end

  def save
    dn = "cn=#{@username},ou=Users,dc=stream,dc=me"
    attributes = {
      :objectClass => ['posixAccount', 'top', 'inetOrgPerson', 'organizationalPerson', 'person', 'shadowAccount'],
      :uid => @username,
      :cn => @username,
      :givenName => @firstname,
      :sn => @lastname.nil? ? '-' : @lastname,
      :mail => @email,
      :uidNumber => '100',
      :gidNumber => '100',
      :homeDirectory => '-',
      :description => '-',
      :shadowInactive => '0',
      :shadowLastChange => '0',
      :userPassword => Net::LDAP::Password.generate(:sha, @password)
    }

    puts @ldap.add(dn, attributes).message

    dn = "cn=#{@username},ou=Devices,dc=stream,dc=me"
    attributes = {
      :objectClass => ['device', 'ipHost', 'top'],
      :cn => @username,
      :ipHostNumber => @ip
    }

    puts @ldap.add(dn, attributes).message

    dn = "cn=Free For All,ou=Profiles,dc=stream,dc=me"
    puts @ldap.connection.add_attribute(dn, :memberUid, @username).inspect
  end

  def valid?
    s = {} 
    @@attributes.each {|v| s[v] = send(v).nil?}
    !s.has_value?(false)
  end

  def firstname=(value)
    tokens = value.split() 
    @firstname = tokens[0] if tokens.length > 0
  end

  def lastname=(value)
    tokens = value.split() 
    @lastname = tokens[1..tokens.length].join(' ') if tokens.length > 1
  end

  def memberships
    filter = Net::LDAP::Filter.eq("memberUid", @username) 
    records = @ldap.connection.search(:base => "ou=Profiles,dc=stream,dc=me", :filter => filter, :attributes => ['cn'])

    groups = []
    records.each { |entry| groups << entry.cn[0]}

    groups
  end

  def self.get(cn)
    record = []
    filter = Net::LDAP::Filter.eq("cn", cn)
    auth = { :method => :simple, :username => "cn=admin,dc=stream, dc=me", :password => "mocoto" }
    Net::LDAP.open(:host => "192.168.100.2", :auth => auth) do |ldap|
      record = ldap.search(:base => "ou=Users,dc=stream,dc=me", :filter => filter)
    end
    entry = record[0]
    params = {
      'login'     => entry.uid[0],
      'password'  => entry.userPassword[0],
      'name'      => "#{entry.givenName[0]} #{entry.sn[0]}",
      'email'     => entry.mail[0]
    }
    User.new(params)
  end
    
end
