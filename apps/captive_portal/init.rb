require 'rubygems'
require 'erubis'
require 'net/ssh'
require 'sinatra/base'
require 'rack-flash'
require 'sinatra/reloader'
require 'app/application'
require 'lib/ldap'
require 'app/captive_portal'


require 'models/user'
