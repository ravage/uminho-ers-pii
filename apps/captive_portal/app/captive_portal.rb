# encoding: utf-8
class CaptivePortal < Application

  get '/?' do
    session[:logged_in] ||= false

    if session[:logged_in]
      redirect 'http://stream.me/users/profile'
    else
      erubis :index
    end
  end

  post '/users/login/?' do
    redirect 'http://stream.me' if session[:logged_in]

    command_result = ''
    user = User.new(params)

    if user.authenticate
      Net::SSH.start( "gatekeeper" , "ravage") do |ssh|
        command_result = ssh.exec!("sudo /sbin/pfctl -t authenticated -T add #{request.ip}")
      end
      session[:logged_in] = true
      session[:user_id] = params['login']
      redirect 'http://stream.me/users/profile'
    else
      flash[:error] = 'Credenciais Inválidas'
      session[:logged_in] = false
      redirect 'http://stream.me/'
    end
  end

  get '/users/logout' do
    redirect 'http://stream.me' unless session[:logged_in]
    user = session.delete(:user_id)
    session[:logged_in] = false
    Net::SSH.start( "gatekeeper" , "ravage") do |ssh|
      command_result = ssh.exec!("sudo /sbin/pfctl -t authenticated -T delete #{request.ip}")
    end
    redirect 'http://stream.me'
  end

  get '/users/register/?' do
    if session[:logged_in]
      redirect 'http://stream.me/users/profile'
    else
      erubis :new_user
    end
  end

  get '/users/profile/?' do
    if session[:logged_in]
      @user = User.get(session[:user_id])
      erubis :profile
    else
      redirect 'http://stream.me'
    end
  end

  post '/users/register' do
    params['ip'] = request.ip
    user = User.new(params) 
    user.authenticate_admin
    user.save
    redirect 'http://stream.me'
  end
end
