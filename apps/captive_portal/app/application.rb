class Application < Sinatra::Base
  use Rack::Flash
  set :static, true
  set :sessions, true
  set :environment, :development  
  set :root, File.dirname(__FILE__)
  set :public, File.join(File.dirname(__FILE__), '../', 'public')
  set :logging, true
 
  configure :development do
    register Sinatra::Reloader
    also_reload "models/*.rb"
  end

end
