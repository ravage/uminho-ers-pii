post '/users/login/?' do
  redirect 'http://stream.me' if session[:logged_in]

  command_result = ''
  user = User.new(params)

  if user.authenticate
    Net::SSH.start( "gatekeeper" , "ravage") do |ssh|
      command_result = ssh.exec!("sudo /sbin/pfctl -t authenticated -T add #{request.ip}")
    end
    session[:logged_in] = true
    session[:user_id] = params['login']
    redirect 'http://stream.me/users/profile'
  else
    flash[:error] = 'Credenciais Inválidas'
    session[:logged_in] = false
    redirect 'http://stream.me/'
  end
end

get '/users/logout' do
  redirect 'http://stream.me' unless session[:logged_in]
  user = session.delete(:user_id)
  session[:logged_in] = false
  Net::SSH.start( "gatekeeper" , "ravage") do |ssh|
    command_result = ssh.exec!("sudo /sbin/pfctl -t authenticated -T delete #{request.ip}")
  end
  redirect 'http://stream.me'
end

