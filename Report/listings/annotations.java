package me.stream.entities;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MediaStream", propOrder = {
  "name",
  "url",
  "key",
  "id"
})

@XmlRootElement(name = "MediaStream")
public class MediaStream {
  @XmlElement(name = "name")
  private String name;
  @XmlElement(name = "url")
  private String url;
  @XmlElement(name = "key")
  private String key;
  @XmlElement(name = "id")
  private String id;

  public MediaStream() {
    name = "Unavailable";
  }
}

