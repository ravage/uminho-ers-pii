  def save
    dn = "cn=#{@username},ou=Users,dc=stream,dc=me"
    attributes = {
      :objectClass => ['posixAccount', 'top', 'inetOrgPerson', 'organizationalPerson', 'person', 'shadowAccount'],
      :uid => @username,
      :cn => @username,
      :givenName => @firstname,
      :sn => @lastname.nil? ? '-' : @lastname,
      :mail => @email,
      :uidNumber => '100',
      :gidNumber => '100',
      :homeDirectory => '-',
      :description => '-',
      :shadowInactive => '0',
      :shadowLastChange => '0',
      :userPassword => Net::LDAP::Password.generate(:sha, @password)
    }

    puts @ldap.add(dn, attributes).message

    dn = "cn=#{@username},ou=Devices,dc=stream,dc=me"
    attributes = {
      :objectClass => ['device', 'ipHost', 'top'],
      :cn => @username,
      :ipHostNumber => @ip
    }

    puts @ldap.add(dn, attributes).message

    dn = "cn=Free For All,ou=Profiles,dc=stream,dc=me"
    puts @ldap.connection.add_attribute(dn, :memberUid, @username).inspect
  end

